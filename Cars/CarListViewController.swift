//
//  ViewController.swift
//  Cars
//
//  Created by Alberto Calderón on 24-08-18.
//  Copyright © 2018 Alberto Calderón. All rights reserved.
//

import UIKit

import RxSwift
import RxCocoa

///Pantalla principal de la aplicación que permite seleccionar marcas y listar modelos
class CarListViewController: UIViewController {
    
    @IBOutlet private var carBrandsCollection: UICollectionView!
    @IBOutlet private var carModelTableview: UITableView!
    @IBOutlet private var overBrandView: UIView!
    @IBOutlet private var overModelView: UIView!
    let disposeBag = DisposeBag()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupBrandObserver()
        CarInformation.shared.getBrands()
        setupBrandCellTapHandling()
        setupModelObserver()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - RX
    /**
     Configura el monitoreo de los cambios en el listado de marcas y lo enlaza a la collectioview
     */
    private func setupBrandObserver() {
        
        CarInformation.shared.brands.asObservable()
            .bind(to:
                self.carBrandsCollection
                .rx
                .items(
                    cellIdentifier: BrandCollectionViewCell.cellIdentifier,
                    cellType: BrandCollectionViewCell.self)) {
                        row, element, cell in
                        cell.configureCell(brand: element)
                        if(row == 1){
                                self.removeOverBrand()
                        }
                        
                }
            .disposed(by: disposeBag)
        
    }
    
    /**
     Configura el monitoreo de los taps en el listado de marcas y solicita acción al CarInformation
     */
    private func setupBrandCellTapHandling(){
        carBrandsCollection
            .rx
            .modelSelected(Brand.self)
            .subscribe(onNext:{
                brand in
                brand.toggleSelected()
                CarInformation.shared.tappedModel(brand: brand)
                self.carBrandsCollection.reloadData()
            })
            .disposed(by: disposeBag)
    }

    /**
     Configura el monitoreo de cambios en el listado de modelos y lo enlaza a la tableview de modelos
     */
    private func setupModelObserver() {
        
        CarInformation.shared.brandListEmpty.subscribe(onNext:{ _ in
            self.showOverModel()
        }).disposed(by: disposeBag)
        
        CarInformation.shared.models.asObservable()
            .bind(to:
                self.carModelTableview
                    .rx
                    .items(
                        cellIdentifier: CarModelTableViewCell.cellIdentifier, cellType: CarModelTableViewCell.self)) {  row, element, cell in
                            cell.configureCell(model: element)
                            if(row == 1){
                                self.removeOverModel()
                            }
            }
            .disposed(by: disposeBag)
        
    }
    
    //MARK: - Animations
    
    /**
     Realiza la animación que oculta de UIView que cubre el listado de marcas
     */
    private func removeOverBrand(){
        UIView.animateKeyframes(withDuration: 0.4, delay: 0.0, options: UIViewKeyframeAnimationOptions(rawValue: 7), animations: {
            self.overBrandView.transform = CGAffineTransform.init(translationX: 0, y: self.overBrandView.frame.height)
        },completion: {_ in
            self.view.sendSubview(toBack: self.overBrandView)
        })
    }
    
    /**
     Realiza la animación que oculta el UIView que cubre el listado de modelos
     */
    private func removeOverModel(){
        UIView.animateKeyframes(withDuration: 0.4, delay: 0.0, options: UIViewKeyframeAnimationOptions(rawValue: 7), animations: {
            self.overModelView.transform = CGAffineTransform.init(translationX: 0, y: self.overModelView.frame.height)
        },completion: {_ in
            self.view.sendSubview(toBack: self.overModelView)
        })
    }
    
    /**
     Realiza la animación que muestra el UIView que cubre el listado de modelos
     */
    private func showOverModel(){
        UIView.animateKeyframes(withDuration: 0.4, delay: 0.0, options: UIViewKeyframeAnimationOptions(rawValue: 7), animations: {
            self.view.bringSubview(toFront: self.overModelView)
            self.overModelView.transform = CGAffineTransform.identity
        },completion: {_ in
            
        })
    }

}

