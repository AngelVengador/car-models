//
//  CarModel.swift
//  Cars
//
//  Created by Alberto Calderón on 24-08-18.
//  Copyright © 2018 Alberto Calderón. All rights reserved.
//


import UIKit
import RxSwift
import Alamofire
import RxAlamofire


/// Modelo de los objetos del servicio models describe un modelo de automóvil
class CarModel {
    var brandId : String
    var created:NSDate
    var id: String
    var modificated : NSDate
    var name: String
    let disposeBag = DisposeBag()
    let brand : Brand
    
    /**
     Inicializador del modelo de auto basado en el json del servicio
     
     - Parameters:
     - jsonDic: Diccionario que se genera desde el json entregado por el servicio
     - brand: El objeto Brand desde el que se realizó la llamada de modelos
     */
    init(jsonDic: NSDictionary, brand:Brand) {
        self.brandId = jsonDic["brand"] as! String
        self.id = jsonDic["id"] as! String
        self.name = jsonDic["name"] as! String
        self.modificated = NSDate(timeIntervalSince1970: jsonDic["modif"] as! Double/1000)
        self.created = NSDate(timeIntervalSince1970: jsonDic["created"] as! Double/1000)        
        self.brand = brand
    }
    
}
