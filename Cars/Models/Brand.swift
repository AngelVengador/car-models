//
//  Brand.swift
//  
//
//  Created by Alberto Calderón on 24-08-18.
//

import UIKit
import RxSwift
import Alamofire
import RxAlamofire

/// Modelo de los objetos del servicio Brands describe una marca de automóvil
class Brand {
    var created:NSDate
    var id: String
    var img:  UIImage
    var modificated : NSDate
    var name: String
    let disposeBag = DisposeBag()
    var imgDownloaded = PublishSubject<Bool>()
    var isSelected = false
    
    /**
     Inicializador de la marca de auto basado en el json del servicio
     
     - Parameters:
     - jsonDic: Diccionario que se genera desde el json entregado por el servicio
     */
    init(jsonDic: NSDictionary) {
        
        self.id = jsonDic["id"] as! String
        self.name = jsonDic["name"] as! String
        self.modificated = NSDate(timeIntervalSince1970: jsonDic["modif"] as! Double/1000)
        self.created = NSDate(timeIntervalSince1970: jsonDic["created"] as! Double/1000)
        self.img = UIImage(named: "car-placeholder.png")!
        let url = jsonDic["img"] as! String
        
        Alamofire.request(url).responseData { response in
            if let data = response.result.value{
                self.img = UIImage(data: data)!
                self.imgDownloaded.onNext(true)
            }
        }
    }
    
    
    /**
     Revisa el estado actual de la selección de la marca y realiza un toggle
     */
    public func toggleSelected(){

        if let index = CarInformation.shared.selectedBrands.value.index(of: self.id) {
            CarInformation.shared.selectedBrands.value.remove(at: index)
            self.isSelected = false
        }else{
            CarInformation.shared.selectedBrands.value.append(self.id)
            self.isSelected = true
        }

    }
    
}
