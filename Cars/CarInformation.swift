//
//  CarInformation.swift
//  Cars
//
//  Created by Alberto Calderón on 24-08-18.
//  Copyright © 2018 Alberto Calderón. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import RxAlamofire
import PromiseKit
import AwaitKit

// Singleton que maneja la información
class CarInformation {
    
    static let shared = CarInformation()
    let brands : Variable<[Brand]> = Variable<[Brand]>([])
    let disposeBag = DisposeBag()
    let models : Variable<[CarModel]> = Variable<[CarModel]>([])
    var carModels : [String:[CarModel]] = ([:])
    let selectedBrands : Variable<[String]> = Variable<[String]>([])
    var brandListEmpty = PublishSubject<Bool>()
    let modelUrl = "https://api01.jooycar.com/api/v1/core/models?brand="
    let brandUrl = "https://api01.jooycar.com/api/v1/core/brands"
    
    
    /**
     Realiza el llamado al servicio de marcas y almacena el resultado en el array de brands
     */
    public func getBrands(){
        
        RxAlamofire.requestJSON(.get, brandUrl)
            .retry(3)
            .subscribe(onNext: { [weak self] (r, json) in
                if let array = json as? NSArray{
                    self?.brands.value = array.map {
                        Brand(jsonDic: $0 as! NSDictionary)
                    }
                }else{
                    print(json)
                }
                }, onError: { _ in
                    print(self)
            })
            .disposed(by: disposeBag)
    }
    
    /**
     Recibe la notificación de que el usuario hizo tap en una marca
     
     - Parameters:
     - brand: El objeto Brand de la marca seleccionada
     */
    public func tappedModel(brand: Brand){
        
        if carModels[brand.id] != nil{
            updateModelList()
        }else{
            callModel(brand: brand)
        }
    }
    
    
    /**
     Recibe el llamado para obtener los modelos de una marca y realiza el llamado a la función asincrona
     
     - Parameters:
     - brand: El objeto Brand para el que se quiere obtener los modelos
     */
    private func callModel(brand: Brand){
        
        async {
            _ = try await(self.requestModels(brand: brand))
        }
        
    }

    /**
     Revisa la lista de marcas seleccionadas y recontruye el listado de modelos en base a ello.
     Dispara el PublicSubject brandListEmpty cuando la lista es vacía

     */
    private func updateModelList(){
        var tempArray  = [CarModel]()
        for brandId in selectedBrands.value{
            if let modelList = carModels[brandId]{
                tempArray += modelList
            }
        }
        if(tempArray.count == 0){
            brandListEmpty.onNext(true)
        }
        models.value = tempArray
    }
    
    
    /**
     Recibe el llamado para obtener los modelos de una marca desde el servicio
     y los almacena en el diccionario de modelos
     
     - Parameters:
     - brand: El objeto Brand para el que se quiere obtener los modelos
     - url: URL donde está el servicio de marcas
     */
    func requestModels(brand: Brand) -> Promise<NSData> {

        return Promise{seal in
            RxAlamofire.requestJSON(.get, self.modelUrl+brand.id)
                .retry(3)
                .debug()
                .subscribe(onNext: { [weak self] (r, json) in
                    
                    if let array = json as? NSArray{
                        self?.carModels[brand.id] = array.map{
                            CarModel(jsonDic: $0 as! NSDictionary, brand: brand)
                        }
                        self?.updateModelList()
                    }else{
                        print(json)
                    }
                    }, onError: { _ in
                        brand.toggleSelected()
                })
                .disposed(by: disposeBag)
            seal.fulfill("ok".data(using: String.Encoding.utf8)! as NSData)
        }
    }
    
}
