//
//  BrandCollectionViewCell.swift
//  
//
//  Created by Alberto Calderón on 24-08-18.
//

import UIKit
import RxSwift
import RxCocoa

// Celda que describe una Brand (Marca de auto)
class BrandCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private var logo: UIImageView!
    @IBOutlet private var title: UILabel!
    @IBOutlet private var selectIndicator: UIView!
    let disposeBag = DisposeBag()
    private var brand: Brand?
    static let cellIdentifier = "brandCollectionViewCell"
    
    /**
     Configura la celda basado en un Brand
     
     - Parameters:
     - brand: Brand que se describe en la celda
     */
    func configureCell(brand: Brand){
        
        selectIndicator.layer.cornerRadius = 5
        
        self.brand = brand
        logo.image = brand.img
        title.text = brand.name
        
        
        
        if brand.isSelected{
            self.title.textColor = UIColor(red: 0, green: 0, blue: 0.5, alpha: 1)
            selectIndicator.alpha = 1
        }else{
            title.textColor = UIColor(white: 0.3, alpha: 1);
            selectIndicator.alpha = 0
        }

        
        brand.imgDownloaded.subscribe(onNext:{ _ in
            self.logo.image = brand.img
        }).disposed(by: disposeBag)
    
    }
    
}
