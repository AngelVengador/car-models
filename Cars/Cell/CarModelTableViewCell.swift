//
//  CarModelTableViewCell.swift
//  Cars
//
//  Created by Alberto Calderón on 24-08-18.
//  Copyright © 2018 Alberto Calderón. All rights reserved.
//

import UIKit
import RxSwift


/// Celda de la tabla de modelos de auto
class CarModelTableViewCell: UITableViewCell {
    
    @IBOutlet private var title: UILabel!
    @IBOutlet private var logo: UIImageView!

    let disposeBag = DisposeBag()
    private var model: CarModel?
    static let cellIdentifier = "carModelCell"
    
    /**
     Configura la celda basado en un CarModel
     
     - Parameters:
     - model: CarModel que se describe en la celda
     */
    func configureCell(model: CarModel){
        self.model = model
        title.text = model.name
        logo.image = model.brand.img
    
    }

}
